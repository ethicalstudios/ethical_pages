<?php
/**
 * @file
 * ethical_pages.features.metatag.inc
 */

/**
 * Implements hook_metatag_export_default().
 */
function ethical_pages_metatag_export_default() {
  $config = array();

  // Exported Metatag config instance: node:oe_main_page.
  $config['node:oe_main_page'] = array(
    'instance' => 'node:oe_main_page',
    'config' => array(),
  );

  return $config;
}
